const automovil = {
   marca: "OTRA",
   modelo: "Yaris",
   descripcion: "Toyota Yaris 2024 Automatico Aire Acondicionado Precio $330,000",
   imagen: "/img/auto.jpg"
};

// Mostrar objeto
console.log("Automovil" + automovil);

console.log("Marca: " + automovil.marca);
console.log("Modelo: " + automovil.modelo);
console.log("Descripcion: " + automovil.descripcion);
console.log("Imagen: " + automovil.imagen);

const modeloElement = document.getElementById("Modelo");
modeloElement.innerHTML = automovil.modelo;

const marcaElement = document.getElementById("marca");
marcaElement.innerHTML = automovil.marca;

const descripcionElement = document.getElementById("descripcion");
descripcionElement.innerHTML = automovil.descripcion;

const imagenElement = document.getElementById("imagen");
imagenElement.src = automovil.imagen;

// Arreglo de objetos
const automoviles = [{
       marca: "TOYOTA",
       modelo: "Yaris",
       descripcion: "Toyota Yaris 2024 Automatico Aire Acondicionado Precio $330,000",
       imagen: "/img/auto.jpg"
   },

   {
       marca: "NISSAN",
       modelo: "Sentra",
       descripcion: "Nissan Sentra 2024 Automatico Aire Acondicionado Precio $330,000",
       imagen: "/img/auto.jpg"
   },

   {
       marca: "FORD",
       modelo: "Bronco",
       descripcion: "Ford Bronco 2024 Automatico Aire Acondicionado Precio $330,000",
       imagen: "/img/auto.jpg"
   }
];

// Mostrar todos los objetos
for (let con = 0; con < automoviles.length; ++con) {
   console.log("Marca: " + automoviles[con].marca);
   console.log("Modelo: " + automoviles[con].modelo);
   console.log("Descripcion: " + automoviles[con].descripcion);
   console.log("Imagen: " + automoviles[con].imagen);
}

const selMarc = document.getElementById('selMarc');
const marcasElements = document.querySelectorAll('#marcas');

selMarc.addEventListener('change', function() {
   for (let i = 0; i < marcasElements.length; i++) {
       marcasElements[i].style.display = 'none';
   }

   let opcion = selMarc.value; // Obtener el valor seleccionado

   const imagenes = [
       ["/img/img1.jpg", "/img/img2.jpg", "/img/img3.png", 
        "/img/high.png", "/img/prius.png", "/img/tacoma.jpg"], // Toyota
       ["/img/altima.jpg", "/img/img5.webp", "/img/img6.webp",
        "/img/path.webp", "/img/frontier.webp", "/img/versa.webp"], // Nissan
       ["/img/bronco.png", "/img/fusion.webp", "/img/mustang.webp",
        "/img/escape.webp", "/img/edgeee.jpg", "/img/explorer.avif"] // Ford
   ];

   const modelos = [
       ["Camry", "RAV4", "Corolla Hatchback", "Highlander", "Prius", "Tacoma"], // Toyota
       ["Altima", "Rogue", "Sentra", "Pathfinder", "Frontier", "Versa"], // Nissan
       ["Bronco", "Fusion", "Mustang", "Escape", "Edge", "Explorer"] // Ford
   ];

   const marcas = [
       ["Toyota", "Toyota", "Toyota","Toyota","Toyota","Toyota"], // Toyota
       ["Nissan", "Nissan", "Nissan", "Nissan", "Nissan", "Nissan"], // Nissan
       ["Ford", "Ford", "Ford", "Ford", "Ford", "Ford"] // Ford
   ];

   const descripciones = [
       ["Toyota Camry 4 Puertas Transmisión Automática $25,000 Credito 20% Pago Inicial",
           "Toyota RAV4 5 Puertas Transmisión Automática $30,000 Credito 25% Pago Inicial",
           "Toyota Corolla Hatchback 5 Puertas Transmisión Estándar $22,000 Credito 15% Pago Inicial",
            "Toyota Highlander 5 Puertas Transmisión Automática $35,000 Crédito 20% Pago Inicial",
              "Toyota Prius 5 Puertas Transmisión Híbrida $28,500 Crédito 25% Pago Inicial",
               "Toyota Tacoma 4 Puertas Transmisión Manual $32,000 Crédito 15% Pago Inicial"], // Toyota
       ["Nissan Altima 4 Puertas Transmisión Automática $27,000 Credito 20% Pago Inicial",
           "Nissan Rogue 5 Puertas Transmisión Automática $31,000 Credito 25% Pago Inicial",
           "Nissan Sentra 4 Puertas Transmisión Estándar $20,000 Credito 15% Pago Inicial",
           "Nissan Pathfinder 5 Puertas Transmisión Automática $33,500 Crédito 20% Pago Inicial",
           "Nissan Frontier Cabina Sencilla 2 Puertas Transmisión Manual $28,000 Crédito 25% Pago Inicial",
           "Nissan Versa 4 Puertas Transmisión Automática $22,500 Crédito 15% Pago Inicial"], // Nissan
       ["Ford Bronco 2024 Automatico Aire Acondicionado Precio $330,000",
           "Ford Fusion 2024 Automatico Aire Acondicionado Precio $330,000",
           "Ford Mustang 2024 Automatico Aire Acondicionado Precio $330,000",
           "Ford Escape 5 Puertas Transmisión Automática $28,500 Crédito 20% Pago Inicial",
           "Ford Edge 5 Puertas Transmisión Automática $31,000 Crédito 25% Pago Inicial",
           "Ford Explorer 5 Puertas Transmisión Automática $35,000 Crédito 15% Pago Inicial"] // Ford
   ];

   const cards = document.querySelectorAll('.card'); // Seleccionar todas las tarjetas

   // Cambiar los valores de cada tarjeta según la marca seleccionada
   for (let i = 0; i < cards.length; i++) {
       cards[i].querySelector('img').src = imagenes[opcion][i];
       cards[i].querySelector('.card h1').textContent = modelos[opcion][i];
       cards[i].querySelector('.card p#marca').textContent = marcas[opcion][i];
       cards[i].querySelector('.card p#descripcion').textContent = descripciones[opcion][i];
   }
});
